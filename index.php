<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Gallery PHP</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	</head>
	<body>
		<header class="text-center">
			<img src="images/logo.svg" class="img-responsive" alt="Responsive Gallery PHP">
		</header>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#colapso">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Responsive Gallery PHP</a>
				</div>
				<div class="collapse navbar-collapse" id="colapso">
					<ul class="menu nav navbar-nav" id="menu">
						<li class="active"><a href="#">Album One</a></li>
						<li><a href="#">Album Two</a></li>
						<li><a href="#">Album Three</a></li>
					</ul>
	
				</div>
			</div>
		</nav>
		<main>Images TODO</main>
		<footer>Pagination</footer>
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script>
			$('.menu li a').click(function(e) {
				console.log(e);
				var $this = $(this);
				if (!$this.parent().hasClass('active')) {
					$not(this).removeClass('active').addClass('inactive');
					$this.parent().addClass('active');
				}
				
        }
				e.preventDefault();
			});
		</script>
	</body>
</html>